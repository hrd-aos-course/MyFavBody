package com.senghuy.myfavbody;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BodyAdapter extends RecyclerView.Adapter<BodyViewHolder> {

    Context mContext;

    // ArrayList for person names
    ArrayList<Body> mDataSet;

    public BodyAdapter(Context context, ArrayList<Body> dataSet) {
        mContext = context;
        mDataSet = dataSet;
    }

    @NonNull
    @Override
    public BodyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_body_layout, parent, false);
        return new BodyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BodyViewHolder holder, int position) {
        holder.mImageViewBody.setImageResource(mDataSet.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}
