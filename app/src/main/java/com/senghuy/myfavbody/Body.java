package com.senghuy.myfavbody;

public class Body {

    private int id;
    private int image;

    public Body() {
    }

    public Body(int id, int image) {
        this.id = id;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Body{" +
                "id=" + id +
                ", image=" + image +
                '}';
    }
}
