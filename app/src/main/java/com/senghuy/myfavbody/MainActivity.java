package com.senghuy.myfavbody;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity{

    RecyclerView mBodyRecyclerView;
    GridLayoutManager gridLayoutManager;
    BodyAdapter mBodyAdapter;
    AppCompatButton btnNext;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initial recycler view
        mBodyRecyclerView = findViewById(R.id.recyclerView);

        // initial layout manager with simple GridLayoutManager that spans two columns
        gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);

        // set layout manager
        mBodyRecyclerView.setLayoutManager(gridLayoutManager);

        // create our dataset
        mBodyRecyclerView = findViewById(R.id.recyclerView);
        ArrayList<Body> mDataSet = new ArrayList<>();
            mDataSet.add(new Body(1, R.drawable.head1));
            mDataSet.add(new Body(2, R.drawable.head2));
            mDataSet.add(new Body(3, R.drawable.head3));
            mDataSet.add(new Body(4, R.drawable.head4));
            mDataSet.add(new Body(1, R.drawable.head5));
            mDataSet.add(new Body(2, R.drawable.head6));
            mDataSet.add(new Body(3, R.drawable.head7));
            mDataSet.add(new Body(4, R.drawable.head8));
            mDataSet.add(new Body(1, R.drawable.head9));
            mDataSet.add(new Body(2, R.drawable.head10));
            mDataSet.add(new Body(3, R.drawable.head11));
            mDataSet.add(new Body(4, R.drawable.head12));

            mDataSet.add(new Body(1, R.drawable.body1));
            mDataSet.add(new Body(2, R.drawable.body2));
            mDataSet.add(new Body(3, R.drawable.body3));
            mDataSet.add(new Body(4, R.drawable.body5));
            mDataSet.add(new Body(5, R.drawable.body6));
            mDataSet.add(new Body(6, R.drawable.body7));
            mDataSet.add(new Body(7, R.drawable.body8));
            mDataSet.add(new Body(8, R.drawable.body9));
            mDataSet.add(new Body(9, R.drawable.body10));
            mDataSet.add(new Body(10, R.drawable.body11));
            mDataSet.add(new Body(11, R.drawable.body12));

            mDataSet.add(new Body(1, R.drawable.legs1));
            mDataSet.add(new Body(2, R.drawable.legs2));
            mDataSet.add(new Body(3, R.drawable.legs4));
            mDataSet.add(new Body(4, R.drawable.legs5));
            mDataSet.add(new Body(5, R.drawable.legs6));
            mDataSet.add(new Body(6, R.drawable.legs7));
            mDataSet.add(new Body(7, R.drawable.legs8));
            mDataSet.add(new Body(8, R.drawable.legs9));
            mDataSet.add(new Body(9, R.drawable.legs10));
            mDataSet.add(new Body(10, R.drawable.legs11));
            mDataSet.add(new Body(11, R.drawable.legs12));

        // initial adapter
        mBodyAdapter = new BodyAdapter(this, mDataSet);

        // set adapter
        mBodyRecyclerView.setAdapter(mBodyAdapter);

        // set Button Next
        btnNext = findViewById(R.id.btn_next);

        View.OnClickListener viewpoint = view -> {
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            startActivity(intent);
        };
        btnNext.setOnClickListener(viewpoint);
    }

}