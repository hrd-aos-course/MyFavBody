package com.senghuy.myfavbody;

import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

public class BodyViewHolder extends RecyclerView.ViewHolder  {

    ImageView mImageViewBody;

    public BodyViewHolder(@NonNull View itemView) {
        super(itemView);
        mImageViewBody = itemView.findViewById(R.id.imageViewItem);

        mImageViewBody.setOnClickListener(v -> {
            Toast.makeText(v.getContext(), "Clicked -> " + getAdapterPosition(),
                    Toast.LENGTH_SHORT).show();

        });
    }
}
